package com.example.quantovocgostadeseries;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private SeekBar mSeekBar;
    private ImageView mImagem;
    private MediaPlayer mediaPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSeekBar = findViewById(R.id.seekBarID);
        mImagem = findViewById(R.id.ImagemID);
        mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.grito);


        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress == 0) {
                    mImagem.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.pouco));

                } else if (progress == 1) {
                    mImagem.setImageDrawable((ContextCompat.getDrawable(MainActivity.this, R.drawable.medio)));

                } else if (progress == 2) {
                    mImagem.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.muito));

                }  else if (progress == 3) {
                    mImagem.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.susto));
                    TocarMusica();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void TocarMusica() {
        mediaPlayer.start();
    }

    private void PauseMusica() {
        mediaPlayer.pause();
    }

    @Override
    protected void onDestroy() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;


        }
        Toast.makeText(MainActivity.this, "Desculpe se voce tomou Susto kkk", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }
}
